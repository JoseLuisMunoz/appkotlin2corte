package com.semtask.util

import com.semtask.models.Rosters
import com.semtask.models.Users
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction


object DatabaseConfig {
    fun init(){
        Database.connect(getConfigHikari())
        createTables()
    }

    private fun createTables() {
        transaction {
            SchemaUtils.create(Users, Rosters)

            if(Users.selectAll().count()==0){
                Users.insert {
                    it[username]= "administrador"
                    it[name]="task"
                    it[password]="administrador"
                    it[email]="josepheszz@gmail.com"
                }
            }
        }

    }

    private fun getConfigHikari(): HikariDataSource {
        val config = HikariConfig()
        config.dataSourceClassName = "org.postgresql.ds.PGSimpleDataSource"
        config.jdbcUrl = "jdbc:/postgres://localhost:5432/task"
        config.username = "task"
        config.password = "task"
        config.maximumPoolSize = 3
        config.validate()

        return HikariDataSource(config)
    }
}