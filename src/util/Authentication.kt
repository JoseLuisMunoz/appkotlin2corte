package com.semtask.util

import com.semtask.entity.UserEntity
import io.ktor.application.ApplicationCall
import io.ktor.auth.authentication

val ApplicationCall.authenticateUser get() = authentication.principal<UserEntity>()