package com.semtask.models

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object Rosters: IntIdTable(){
    val title= varchar("title", length = 45)
    val date= varchar("date-task", length = 45)
    val description =varchar("description", length = 100)
    val usertask=reference("user", Users)
}

class RosterDAO(id: EntityID<Int>): IntEntity(id){
    companion object : IntEntityClass<RosterDAO>(Rosters)

    var title by Rosters.title
    var date by Rosters.date
    var descrip by Rosters.description
    var usertask by UserDAO referencedOn Rosters.usertask
}