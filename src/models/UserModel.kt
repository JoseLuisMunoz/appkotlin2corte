package com.semtask.models

import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.IntIdTable

object Users: IntIdTable() {
    val username= varchar("username", length = 45).uniqueIndex()
    val name= varchar("name", length = 60)
    val password=varchar("password", length = 45)
    val email=varchar("email", length = 45)
}

class UserDAO(id: EntityID<Int>): IntEntity(id){
    companion object : IntEntityClass<UserDAO>(Users)

    var username by Users.username
    var name by Users.name
    var password by Users.password
    var email by Users.email
}