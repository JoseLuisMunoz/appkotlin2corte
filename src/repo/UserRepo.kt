package com.semtask.repo

import com.semtask.entity.UserEntity
import com.semtask.models.UserDAO
import com.semtask.models.Users
import io.ktor.auth.UserPasswordCredential
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.transactions.transaction

class UserRepo {

    fun findUserByCredentials(credentials: UserPasswordCredential): UserEntity? =
        transaction {
            UserDAO.find {
                (Users.username eq credentials.name) and (Users.password eq credentials.password)
            }.map { toUser(it) }.firstOrNull()
        }

    fun insertUser(user: UserEntity): UserEntity?{
        var createdUser: UserEntity? = null
        transaction {
            val userDAO = UserDAO.new {
                username= user.username
                name= user.name
                password= user.password!!
                email= user.email
            }
            createdUser= toUser(userDAO)
        }
        return createdUser
    }

    fun getUsers(): List<UserEntity> = transaction{
        UserDAO.all().map { toUser(it) }
    }

    fun findUserByUsername(username: String): UserEntity=
        transaction {
            UserDAO.find{
                Users.username eq username
            }.map { toUser(it) }.first()
        }


    companion object{
        fun toUser(userDAO: UserDAO)=UserEntity(
            id= userDAO.id.value,
            username = userDAO.username,
            name = userDAO.name,
            password = userDAO.password,
            email = userDAO.email
        )
    }
}