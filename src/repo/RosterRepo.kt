package com.semtask.repo

import com.semtask.entity.RosterEntity
import com.semtask.models.RosterDAO
import com.semtask.models.UserDAO
import org.jetbrains.exposed.sql.transactions.transaction

class RosterRepo {

    fun getAllRoster(): List<RosterEntity> = transaction{
        RosterDAO.all().map { toRoster(it) }
    }

    fun getAllRosterUser(userId: Int): List<RosterEntity> = transaction {
        RosterDAO.find { com.semtask.models.Rosters.usertask eq userId }.sortedBy { it.id }.map { toRoster(it) }
    }

    fun insertRosters(roster: RosterEntity, id: Int): RosterEntity? {
        var createdRoster: RosterEntity? = null

        transaction {
            val insertRoster = RosterDAO.new {
                title = roster.title
                date = roster.date
                descrip = roster.descrip
                usertask = UserDAO.get(id)

            }
            createdRoster = toRoster(insertRoster)
        }
        return createdRoster
    }

    companion object{
        fun toRoster(roster: RosterDAO): RosterEntity= RosterEntity(
            id = roster.id.value,
            title = roster.title,
            date = roster.date,
            descrip = roster.descrip,
            usertask = UserRepo.toUser(roster.usertask)
        )
    }
}