package com.semtask.entity

data class RosterEntity (
    val id: Int = 0,
    val title: String = "",
    val date: String= "",
    val descrip: String= "",
    val usertask: UserEntity? = null
)