package com.semtask.entity

import io.ktor.auth.Principal


data class UserEntity(
    val id: Int,
    val username: String,
    val name: String,
    val password: String? = null,
    val email: String

): Principal