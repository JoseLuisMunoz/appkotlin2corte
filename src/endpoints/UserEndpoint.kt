package com.semtask.endpoints

import com.semtask.entity.UserEntity
import com.semtask.repo.UserRepo
import com.semtask.util.Message
import com.semtask.util.authenticateUser
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route

fun Route.userEndpoint(userRepo: UserRepo){
    route("users"){

        get {
            call.authenticateUser!!
            call.respond(HttpStatusCode.OK, userRepo.getUsers())
        }

        post ("create"){
            val candidate = call.receive<UserEntity>()
            val createUser= userRepo.insertUser(candidate)

            if(createUser== null){
                call.respond(HttpStatusCode.NotAcceptable, Message("No se pudo crear el usuario"))
            }else{
                call.respond(HttpStatusCode.OK, createUser)
            }

        }
    }

}