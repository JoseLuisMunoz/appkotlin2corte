package com.semtask.endpoints

import com.semtask.auth.JWTBase
import com.semtask.entity.UserEntity
import com.semtask.repo.UserRepo
import com.semtask.util.Message
import io.ktor.application.call
import io.ktor.auth.UserPasswordCredential
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.post

fun Route.auth(userRepo: UserRepo) {

    post("login") {

        val credentials = call.receive<UserPasswordCredential>()
        val user = userRepo.findUserByCredentials(credentials)

        if (user == null) {
            call.respond(HttpStatusCode.NotFound, Message(message = "Credenciales inválidas"))
        } else {
            val token = JWTBase.makeToken(user)
            call.respond(HttpStatusCode.OK, mapOf("jwtKey" to token))
        }
    }

    post("register") {
        val user = call.receive<UserEntity>()
        val createdUser = userRepo.insertUser(user)

        if (createdUser == null) {
            call.respond(HttpStatusCode.NotAcceptable, Message(message = "No se pudo crear el usuario"))
        } else {
            call.respond(HttpStatusCode.OK, createdUser)
        }
    }
}