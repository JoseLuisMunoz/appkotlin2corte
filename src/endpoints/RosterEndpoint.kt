package com.semtask.endpoints

import com.semtask.entity.RosterEntity
import com.semtask.repo.RosterRepo
import com.semtask.util.Message
import com.semtask.util.authenticateUser
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.route

fun Route.rosterEndpoint(rosterRepo: RosterRepo) {

    route("rosters"){
        get {
            var userId = call.authenticateUser!!.id
            call.respond(HttpStatusCode.OK, rosterRepo.getAllRosterUser(userId))
        }

        post("create") {
            call.authenticateUser!!
            val roster = call.receive<RosterEntity>()
            var userId = call.authenticateUser!!.id
            val createRoster = rosterRepo.insertRosters(roster, userId)

            if(createRoster == null){
                call.respond(HttpStatusCode.NotAcceptable, Message("No se pudo crear la tarea"))
            } else {
                call.respond(HttpStatusCode.OK, createRoster)
            }
        }
    }
}