package com.semtask

import com.semtask.auth.JWTBase
import com.semtask.endpoints.auth
import com.semtask.endpoints.rosterEndpoint
import com.semtask.endpoints.userEndpoint
import com.semtask.repo.RosterRepo
import com.semtask.repo.UserRepo
import com.semtask.util.DatabaseConfig
import io.ktor.application.*
import io.ktor.auth.Authentication
import io.ktor.auth.*
import io.ktor.auth.authenticate
import io.ktor.auth.jwt.jwt
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.http.*
import kotlinx.html.*
import kotlinx.css.*
import io.ktor.features.*
import io.ktor.gson.gson

fun main(args: Array<String>): Unit = io.ktor.server.tomcat.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    DatabaseConfig.init()

    install(CallLogging)
    install(DefaultHeaders)
    install(AutoHeadResponse)
    install(CORS) {
        anyHost()
    }

    val userRepo = UserRepo()
    val rosterRepo = RosterRepo()

    install(Authentication) {
        jwt {
            verifier(JWTBase.verifier)
            realm = JWTBase.issuer
            validate {
                it.payload.getClaim("username").asString()?.let(userRepo::findUserByUsername)
            }
        }
    }

    install(ContentNegotiation){
        gson {
            setPrettyPrinting()
        }
    }

    install(Routing){

        auth(userRepo)
        authenticate {
            userEndpoint(userRepo)
            rosterEndpoint(rosterRepo)
        }

    }

    /*
    routing {
        get("/") {
            call.respondText("HELLO WORLD!", contentType = ContentType.Text.Plain)
        }



        get("/html-dsl") {
            call.respondHtml {
                body {
                    h1 { +"HTML" }
                    ul {
                        for (n in 1..10) {
                            li { +"$n" }
                        }
                    }
                }
            }
        }

        get("/styles.css") {
            call.respondCss {
                body {
                    backgroundColor = Color.red
                }
                p {
                    fontSize = 2.em
                }
                rule("p.myclass") {
                    color = Color.blue
                }
            }
        }
    }*/
}

fun FlowOrMetaDataContent.styleCss(builder: CSSBuilder.() -> Unit) {
    style(type = ContentType.Text.CSS.toString()) {
        +CSSBuilder().apply(builder).toString()
    }
}

fun CommonAttributeGroupFacade.style(builder: CSSBuilder.() -> Unit) {
    this.style = CSSBuilder().apply(builder).toString().trim()
}

suspend inline fun ApplicationCall.respondCss(builder: CSSBuilder.() -> Unit) {
    this.respondText(CSSBuilder().apply(builder).toString(), ContentType.Text.CSS)
}
